$.material.init();

var json = {
    title: "How Well do you Handle Change and Uncertainty?",
    showProgressBar: "top",
    pages: [{
        questions: [{
            type: "matrix",
            name: "When listening to another person, I...",
            title: "Please select 1 for 'doesn’t describe me at all', 2 for 'describes me to a small extent' and 3 for 'describes me to a moderate extent, or 4 for ' describes me very well' to indicate how consistently you use the described behavior in the workplace",
            columns: [{
                value: 1,
                text: "1"
            }, {
                value: 2,
                text: " 2 "
            }, {
                value: 3,
                text: " 3 "
            }, {
                value: 4,
                text: " 4 "
            }],
            
            rows: [{
                value: "expect",
                text: "I expect change and am seldom surprised when it happens."
            }, {
                value: "flexible",
                text: "I always try to be flexible and adaptable."
            }, {
                value: "handling",
                text: "I am very good at handling stressful situations."
            }, {
                value: "challenge",
                text: "I welcome challenges."
            },{
                value: "situation",
                text: "I look for the good in any situation."
            }, {
                value: "maintain",
                text: "I try to maintain a good support system."
            }, {
                value: "uncover",
                text: "When things change, I seek to uncover the facts and what they mean to me."
            }, {
                value: "learn",
                text: "I try to learn from my experiences."
            }, {
                value: "touch",
                text: "In stressful situations, I am in touch with my feelings."
            }, {
                value: "grow",
                text: " I believe change offers me and my organization opportunities to learn and grow."
            }]
        }]
        }]
   }     
Survey.defaultBootstrapMaterialCss.navigationButton = "btn btn-green";
Survey.defaultBootstrapMaterialCss.rating.item = "btn btn-default my-rating";
Survey.Survey.cssType = "bootstrapmaterial";

var survey = new Survey.Model(json);

survey.onComplete.add(function(result) {

	var sum = 0;
	var rows = Object.values(result.data);
	for(var i = 0; i < rows.length; i++){
		var row = Object.values( rows[i] );
		for(var j = 0; j < row.length; j++) {
			var val = row[j];
			sum += parseInt(val);
		}
	}


function myFunction(sum) 

{
    var scoring;
    if (sum <= 40 && sum >= 36) {
        scoring = " You handle change very well! You can use what you learn in this course to become even better.";
    } else if (sum <= 35 && sum >= 31) {
        scoring = "You handle change well, but you can still improve.";
    } else if (sum <= 30 && sum >= 26) {
        scoring = "You are on your way to being able to handle change successfully, but there are some areas you need to work on. ";
    } else if (sum <= 25 && sum >=21) {
        scoring = "Your ability to handle change is not bad, but you could do better.";
    } else if (sum <= 20 && sum >=16) {
        scoring = "Your ability to handle change could use improvement.";
    } else if (sum <= 15 && sum >=10) {
        scoring = "You may have difficulty handling change and uncertainty.";
    } else {
        scoring = "Error";
    }
    
document.querySelector('#result').innerHTML = "Your Score " + sum + "<br>" + "You Are a(n) " + scoring;

} 

myFunction(sum);

});
       
survey.render("surveyElement");